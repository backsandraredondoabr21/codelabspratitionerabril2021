package net.techu.clienteapi.controller;

import net.techu.clienteapi.model.Frase;
import net.techu.clienteapi.model.Quote;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class QuoteController {

    @GetMapping("/frase")
    public ResponseEntity<Quote> getQuote() {
        RestTemplate template = new RestTemplate();
        Quote resultado = template.getForObject("https://quoters.apps.pcfone.io/api/random", Quote.class);
        return new ResponseEntity<Quote>(resultado, HttpStatus.OK);
    }

    @GetMapping("/frasev2")
    public ResponseEntity<Frase> getQuotePersonalizada() {
        RestTemplate template = new RestTemplate();
        Quote resultado = template.getForObject("https://quoters.apps.pcfone.io/api/random", Quote.class);
        Frase frase = new Frase();
        frase.setTipo(resultado.getType());
        frase.setValor(resultado.getValue().getQuote());
        return new ResponseEntity<Frase>(frase, HttpStatus.OK);
    }
}
