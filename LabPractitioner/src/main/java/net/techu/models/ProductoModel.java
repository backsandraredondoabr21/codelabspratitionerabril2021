package net.techu.models;

import java.util.List;
import java.util.Objects;

public class ProductoModel {
    private long id;
    private String descripcion;
    private double precio;

    public ProductoModel() {
    }

    public ProductoModel(long id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public ProductoModel(long id, String descripcion, double precio, List<UserModel> users) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductoModel{");
        sb.append("id=").append(id);
        sb.append(", descripcion='").append(descripcion).append('\'');
        sb.append(", precio=").append(precio);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductoModel)) return false;
        ProductoModel productoModel = (ProductoModel) o;
        return getId() == productoModel.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
