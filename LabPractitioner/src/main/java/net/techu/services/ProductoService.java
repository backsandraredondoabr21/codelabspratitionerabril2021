package net.techu.services;

import net.techu.models.ProductoModel;
import net.techu.models.UserModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoService {
    private List<ProductoModel> dataList = new ArrayList<ProductoModel>();

    public ProductoService() {
        dataList.add(new ProductoModel(1, "producto 1", 100.50));
        dataList.add(new ProductoModel(2, "producto 2", 150.00));
        dataList.add(new ProductoModel(3, "producto 3", 100.00));
        dataList.add(new ProductoModel(4, "producto 4", 50.75));
        dataList.add(new ProductoModel(5, "producto 5", 120.00));
        List<UserModel> users = new ArrayList<>();
        users.add(new UserModel("1"));
        users.add(new UserModel("3"));
        users.add(new UserModel("5"));
        //dataList.get(1).setUsers(users);
    }

    // Consultar Productos
    public List<ProductoModel> getProductos() {
        return dataList;
    }

}
