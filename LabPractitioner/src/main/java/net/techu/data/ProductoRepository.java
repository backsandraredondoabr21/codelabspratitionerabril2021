package net.techu.data;

import net.techu.models.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.ArrayList;
import java.util.List;

public interface ProductoRepository extends MongoRepository<ProductoMongo, String> {
    @Query("{'descripcion':?0}")
    public List<ProductoMongo> findByDescripcion(String descripcion);

    @Query("{'precio': {$gt: ?0, $lt: ?1}}")
    public List<ProductoMongo> findByPrecio(double minimo, double maximo);

}
