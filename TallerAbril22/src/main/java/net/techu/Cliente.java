package net.techu;

public class Cliente {


    private String nombre;
    private String apellido;
    private String ciudad;


    public Cliente(String nombre, String apellido, String ciudad) {

        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setCiudad(ciudad);
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
