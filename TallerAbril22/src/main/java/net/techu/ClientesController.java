package net.techu;

import net.techu.data.ClienteMongo;
import net.techu.data.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
public class ClientesController {

    @Autowired
    private ClienteRepository repository;

    @GetMapping(value = "/clientes", produces = "application/json")
    public ResponseEntity<List<ClienteMongo>> obtenerListado()
    {
        List<ClienteMongo> lista = repository.findAll();
        return new ResponseEntity<List<ClienteMongo>>(lista, HttpStatus.OK);
    }

    /* Add nuevo cliente */
    @PostMapping(value = "/clientes", produces="application/json")
    public ResponseEntity<String> addcliente(@RequestBody ClienteMongo clienteMongo) {

        System.out.println("Estoy en añadir ClientesController");
        ClienteMongo resultado=repository.insert(clienteMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }
    // PUT Modificar cliente
    @PutMapping("/clientes/{nombre}")
    public ResponseEntity updateProducto(@PathVariable String nombre, @RequestBody ClienteMongo clienteMongo) {
        System.out.println("Estoy actualizando cliente");

        Optional<ClienteMongo> resultado=repository.findByNombre(nombre);
        if(resultado.isPresent()){

            resultado.get().apellido=clienteMongo.apellido;
            resultado.get().ciudad=clienteMongo.ciudad;
        }
        ClienteMongo guardado=repository.save(resultado.get());
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    //Delete Eliminar Clientes
    @DeleteMapping(value="/clientes/{nombre}")
    public ResponseEntity<String> deleteProducto(@PathVariable String nombre)
    {
        repository.deleteAll();
        return new ResponseEntity<String>("Cliente borrado", HttpStatus.OK);
    }



}
