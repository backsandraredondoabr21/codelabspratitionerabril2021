package net.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;
@Document("ClientesSandraR")
public class ClienteMongo {
    public String nombre;
    public String apellido;
    public String ciudad;


    public ClienteMongo() {}

    public ClienteMongo(String nombre, String apellido,String ciudad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return String.format("Cliente [nombre=%s, apellido=%s, ciudad=%s]", nombre, apellido,ciudad);
    }

}
