package net.techu.demosswagger.data;

public class Producto {

    private String nombre;
    private String id;
    private double precio;
    private String categoria;

    public Producto(String nombre, String id, double precio, String categoria) {
        this.nombre = nombre;
        this.id = id;
        this.precio = precio;
        this.categoria = categoria;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
