package net.techu.demosswagger;

import net.techu.demosswagger.data.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class CabecerasController {

    @GetMapping("/saludar")
    public ResponseEntity<String> saludar(@RequestHeader("accept-language") String idioma)
    {
        return new ResponseEntity<String>("Saludo", HttpStatus.OK);
    }

    @GetMapping("/calcular")
    public ResponseEntity<Integer> calcular(@RequestHeader("min-numero") Integer numero)
    {
        return new ResponseEntity<Integer>(numero *2, HttpStatus.OK);
    }

    @GetMapping("/gestionarCabeceras")
    public ResponseEntity<String> gestionarCabeceras(@RequestHeader Map<String, String> cabeceras)
    {
        cabeceras.forEach((clave, valor) -> {
            System.out.println(String.format("Cabecera '%s' = %s", clave, valor));
        });
        return new ResponseEntity<String>(String.format("Total de cabeceras: %d", cabeceras.size()), HttpStatus.OK);
    }

    @GetMapping("/gestionarCabecerasMulti")
    public ResponseEntity<String> gestionarCabecerasMulti(@RequestHeader MultiValueMap<String, String> cabeceras)
    {
        cabeceras.forEach((clave, valor) -> {
            System.out.println(String.format("Cabecera '%s' = %s", clave, valor.stream().collect(Collectors.joining("|"))));
        });
        return new ResponseEntity<String>(String.format("Total de cabeceras: %d", cabeceras.size()), HttpStatus.OK);
    }

    @GetMapping("/cabeceraOpcional")
    public ResponseEntity<String> cabeceraOpcional(@RequestHeader(value = "usuario", required = false) String usuario)
    {
        return new ResponseEntity<String>(String.format("¿Tiene info la cabecera? %s", (usuario == null ? "No" : "Sí")), HttpStatus.OK);
    }
}
