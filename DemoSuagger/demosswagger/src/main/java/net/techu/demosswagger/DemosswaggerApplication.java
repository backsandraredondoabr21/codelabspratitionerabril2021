package net.techu.demosswagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemosswaggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemosswaggerApplication.class, args);
	}

}
