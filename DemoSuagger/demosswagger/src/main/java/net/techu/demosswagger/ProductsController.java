package net.techu.demosswagger;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.techu.demosswagger.data.Producto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductsController {

    private ArrayList<Producto> listaProductos;

    public ProductsController() {
        listaProductos = new ArrayList<Producto>();
        listaProductos.add(new Producto("Producto 1", "Producto 1", 34.55, "CAT1"));
        listaProductos.add(new Producto("Producto 23", "Producto 2", 34.56, "CAT2"));
        listaProductos.add(new Producto("Producto 50", "Producto 3", 800.20, "CAT3"));
    }

    @GetMapping("/productos")
    public ResponseEntity<ArrayList<Producto>> getListaProductos() {
        return new ResponseEntity<ArrayList<Producto>>(listaProductos, HttpStatus.OK);
    }

    @PostMapping("/productos")
    @ApiOperation(value="Crear producto", notes="Este método crea un producto")
    public ResponseEntity<Producto> addProducto(@ApiParam(name="producto",
            type="Producto",
            value="producto a crear",
            example="PROD1",
            required = true) @RequestBody Producto productoNuevo)
    {
        listaProductos.add(productoNuevo);
        return new ResponseEntity<Producto>(productoNuevo, HttpStatus.CREATED);
    }

    @PostMapping("/productos/byname")
    @ApiOperation(value="Crear producto por nombre", notes="Este método crea un producto solo con el nombre")
    public ResponseEntity<Producto> addProductoByName(@ApiParam(name="nombre",
            type="String",
            value="nombre del producto a crear",
            example="PR1",
            required = true) @RequestParam String nombre)
    {
        Producto productoNuevo = new Producto("321", nombre, 125, "CATY");
        listaProductos.add(productoNuevo);
        return new ResponseEntity<Producto>(productoNuevo, HttpStatus.CREATED);
    }
}
