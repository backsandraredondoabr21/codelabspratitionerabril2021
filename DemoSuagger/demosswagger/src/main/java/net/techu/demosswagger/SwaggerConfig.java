package net.techu.demosswagger;

import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

public class SwaggerConfig {
    @Bean
    public Docket apiDocket() {
        /* Detalle de los métodos de la API */
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("net.techu.demoswagger"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo() {
        return new ApiInfo("API de productos",
                "API que gestiona productos",
                "1.0",
                "http://www.techu.net",
                new Contact("Sandra", "www.sandra.com", "sandra@correo.es"),
                "LICENCIA",
                "URL LICENCIA",
                Collections.emptyList());
    }
}
